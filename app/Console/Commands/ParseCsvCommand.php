<?php

namespace App\Console\Commands;

use App\Http\Repositories\CustomerRepository;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ParseCsvCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:csv {filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse csv file to table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $customerRepository = new CustomerRepository;

        $parseFileName = $this->argument('filename');
        $parseFile = storage_path("/excel-files/pending/{$parseFileName}");

        $spreadsheet = IOFactory::load($parseFile);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        array_shift($sheetData);

        $errorRows = [];

        foreach ($sheetData as $key => $row) {

            // NAME/SURNAME
            $nameData = explode(' ', $row[1]);
            $name = $nameData[0];
            $surname = $nameData[1];

            // VALIDATION
            $data = Validator::make([
                'email' => $row[2],
                'age' => intval(preg_replace('/[^0-9]+/', '', $row[3]))
            ], [
                'age'   => 'integer|between:18,99',
                'email' => 'email:rfc,dns',
            ]);

            // ADD ERROR ROW TO ARRAY
            if ($data->fails()) {
                $localErrors = $data->errors();

                $row[5] = $localErrors->keys()[0];
                $errorRows[] = $row;

                continue;
            }

            $email = $row[2];
            $age = intval($row[3]);
            $birhDate = Carbon::now()->subYear($age);
            $countryCode = config('countries')[$row[4]] ?? null;
            if (!$countryCode) $location = 'Unknown';
            else $location = $row[4];

            $customerRepository->updateOrCreate(['email' => $email], [
                'name'    => $name,
                'surname' => $surname,
                'email'   => $email,
                'age'     => $birhDate,
                'location' => $location,
                'country_code' => $countryCode
            ]);
        }


        $errorSpreadsheet = new Spreadsheet;
        $activeSheet = $errorSpreadsheet->getActiveSheet();
        $activeSheet->setTitle('Позиции с ошибками')->fromArray($errorRows);

        $writer = IOFactory::createWriter($errorSpreadsheet, "Xlsx");
        $filePath = 'excel-files/failed/';
        $filename = uniqid() . '-' . time() . ".xlsx";
        $savePath = storage_path($filePath) . $filename;
        $writer->save($savePath);
    }
}
