<?php


namespace App\Http\Repositories;

use App\Models\Customer;

class CustomerRepository extends BaseRepository
{
    protected $model = Customer::class;
}
