<?php


namespace App\Http\Repositories;

use Illuminate\Database\Eloquent\Builder;

class BaseRepository
{
    protected $model;

    public function updateOrCreate(...$args)
    {
        return $this->query()->updateOrCreate(...$args);
    }

    public function create($data)
    {
        return $this->query()->create($data);
    }

    public function query(): Builder
    {
        return $this->model::query();
    }
}
